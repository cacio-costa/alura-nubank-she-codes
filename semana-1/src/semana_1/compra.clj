(ns semana-1.compra
  (:require [semana-1.data :as data]))

(defn nova-compra [data, valor, estabelecimento, categoria]
  {:data            data
   :valor           valor
   :estabelecimento estabelecimento
   :categoria       categoria})


(defn totaliza-valor-de-compras [compras]
  (reduce + (map :valor compras)))


(defn lista-valor-gasto-por-categoria [compras]
  (->> (group-by :categoria compras)
       (map (fn [[categoria compras-agrupadas]]
              {:categoria categoria
               :total     (totaliza-valor-de-compras compras-agrupadas)}))))


(defn filtra-compras-no-mes [compras, mes]
  (filter #(= mes (data/mes-da-data (:data %))) compras))


(defn valor-da-fatura-no-mes [compras, mes]
  (totaliza-valor-de-compras (filtra-compras-no-mes compras mes)))


(defn busca-compras-por-valor [compras valor]
  (filter #(>= (:valor %) valor) compras))


(defn busca-compras-por-estabelecimento [compras estabelecimento]
  (filter #(= estabelecimento (:estabelecimento %)) compras))
