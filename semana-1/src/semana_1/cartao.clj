(ns semana-1.cartao)

(defn novo-cartao [numero, cvv, validade, limite]
  {:numero   numero
   :cvv      cvv
   :validade validade
   :limite   limite})