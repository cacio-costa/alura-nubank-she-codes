(ns semana-1.data)

(defn- fn-para-parte-da-data [expressao-regular]
  (fn [data]
    (second (re-matches expressao-regular data))))

(def dia-da-data (fn-para-parte-da-data #"(\d{2})/\d{2}/\d{4}"))
(def mes-da-data (fn-para-parte-da-data #"\d{2}/(\d{2})/\d{4}"))
(def ano-da-data (fn-para-parte-da-data #"\d{2}/\d{2}/(\d{4})"))
