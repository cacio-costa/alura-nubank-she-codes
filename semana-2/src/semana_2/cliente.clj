(ns semana-2.cliente
  (:require [schema.core :as s]
            [semana-2.schema-util :as su]))

;(def CPF (s/pred #(re-matches #"\d{3}\.\d{3}\.\d{3}-\d{2}" %)))
(def CPF #"\d{3}\.\d{3}\.\d{3}-\d{2}")

(def Cliente {:nome  s/Str
              :cpf   CPF
              :email s/Str})

(s/defn novo-cliente :- Cliente
  [nome :- s/Str, cpf :- CPF, email :- s/Str]
  {:nome  nome
   :cpf   cpf
   :email email})

