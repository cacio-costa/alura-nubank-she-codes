(ns semana-2.data
  (:require [schema.core :as s]
            [semana-2.schema-util :as su]))

(def Data #"\d{2}/\d{2}/\d{4}")

(defn- fn-para-parte-da-data [expressao-regular]
  (s/fn [data :- Data]
    (second (re-matches expressao-regular data))))

(def dia-da-data (fn-para-parte-da-data #"(\d{2})/\d{2}/\d{4}"))
(def mes-da-data (fn-para-parte-da-data #"\d{2}/(\d{2})/\d{4}"))
(def ano-da-data (fn-para-parte-da-data #"\d{2}/\d{2}/(\d{4})"))

