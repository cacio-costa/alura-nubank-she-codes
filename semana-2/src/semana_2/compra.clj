(ns semana-2.compra
  (:require [schema.core :as s]
            [semana-2.data :as data]
            [semana-2.schema-util :as su]))

(def Compra {:data            data/Data
             :valor           su/NumeroPositivo
             :estabelecimento s/Str
             :categoria       s/Str})

(def ComprasAgrupadas {s/Str [Compra]})
(def CategoriasTotalizadas {s/Str su/MaiorOuIgualAZero})


(s/defn nova-compra :- Compra
  [data :- data/Data
   valor :- su/NumeroPositivo
   estabelecimento :- s/Str
   categoria :- s/Str]

  {:data            data
   :valor           valor
   :estabelecimento estabelecimento
   :categoria       categoria})


(s/defn totaliza-valor-de-compras :- su/MaiorOuIgualAZero
  [compras :- [Compra]]
  (reduce + (map :valor compras)))


(s/defn agrupa-por-categoria :- ComprasAgrupadas
  [compras :- [Compra]]
  (group-by :categoria compras))


(s/defn lista-valor-gasto-por-categoria :- CategoriasTotalizadas
  [compras :- [Compra]]
  (->> (group-by :categoria compras)
       (reduce (fn [acumulador [categoria compras-agrupadas]]
                 (assoc acumulador categoria (totaliza-valor-de-compras compras-agrupadas)))
               {})))


(s/defn filtra-compras-no-mes :- [Compra]
  [compras :- [Compra], mes :- s/Str]
  (filter #(= mes (data/mes-da-data (:data %))) compras))


(s/defn valor-da-fatura-no-mes :- su/MaiorOuIgualAZero
  [compras :- [Compra], mes :- s/Str]
  (totaliza-valor-de-compras (filtra-compras-no-mes compras mes)))


(s/defn busca-compras-por-valor :- [Compra]
  [compras :- [Compra], valor :- su/MaiorOuIgualAZero]
  (filter #(>= (:valor %) valor) compras))


(s/defn busca-compras-por-estabelecimento :- [Compra]
  [compras :- [Compra], estabelecimento :- s/Str]
  (filter #(= estabelecimento (:estabelecimento %)) compras))


