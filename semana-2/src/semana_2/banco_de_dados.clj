(ns semana-2.banco-de-dados
  (:require [semana-2.compra :as compra]
            [schema.core :as s]))

(defn inicializa-banco []
  (def compras-realizadas [(compra/nova-compra "01/01/2001", 1500.0M, "Outback", "Alimentação")
                           (compra/nova-compra "02/01/2001", 40.0M, "Netflix", "Entretenimento")

                           (compra/nova-compra "01/02/2001", 400.0M, "Cocobambu", "Alimentação")
                           (compra/nova-compra "02/02/2001", 260.0M, "Posto de gasolina", "Combustível")
                           (compra/nova-compra "03/02/2001", 100.0M, "Pizzaria", "Alimentação")

                           (compra/nova-compra "01/03/2001", 150.0M, "Aramis", "Vestuário")
                           (compra/nova-compra "02/03/2001", 500.0M, "Centauro", "Vestuário")
                           (compra/nova-compra "03/03/2001", 30.0M, "Barbearia", "Beleza")
                           (compra/nova-compra "04/03/2001", 99.0M, "Academia", "Saúde")]))

(s/defn lista-compras-realizadas :- [compra/Compra] []
  compras-realizadas)


(s/defn salva-compra :- [compra/Compra]
  [compra :- compra/Compra]
  (def compras-realizadas (conj compras-realizadas compra))
  (lista-compras-realizadas))


