(ns semana-2.schema-util
  (:require [schema.core :as s]))

(def MaiorOuIgualAZero (s/pred #(>= % 0)))
(def NumeroPositivo (s/pred pos?))
