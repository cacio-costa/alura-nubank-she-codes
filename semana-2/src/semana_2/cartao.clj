(ns semana-2.cartao
  (:require [schema.core :as s]
            [semana-2.schema-util :as su]))

(def NumeroCartao #"\d{4} \d{4} \d{4} \d{4}")
(def CVV (s/pred #(and (>= % 0) (<= % 999))))
(def ValidadeCartao #"\d{2}/\d{4}")


(def Cartao {:numero   NumeroCartao
             :cvv      CVV
             :validade ValidadeCartao
             :limite   su/MaiorOuIgualAZero})

(s/defn novo-cartao :- Cartao
  [numero :- NumeroCartao
   cvv :- CVV
   validade :- ValidadeCartao
   limite :- su/MaiorOuIgualAZero]
  {:numero   numero
   :cvv      cvv
   :validade validade
   :limite   limite})

