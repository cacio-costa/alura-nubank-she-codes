(ns semana-2.core
  (:require [semana-2.cliente :as cliente]
            [semana-2.cartao :as cartao]
            [semana-2.compra :as compra]
            [semana-2.banco-de-dados :as bd]
            [schema.core :as s])
  (:use clojure.pprint))

(s/set-fn-validation! true)

; Inicializa banco de dados fictício
(bd/inicializa-banco)

; Item 1 - Representar dados de um cliente
(pprint (cliente/novo-cliente "Bob" "004.636.051-44" "b@b.com"))

; Item 2 - representar dados de um cartão
(pprint (cartao/novo-cartao "1234 2345 4567 6789" 321 "02/2022" 3000.0M))

; Item 3 - Listar compras realizadas
(pprint (bd/lista-compras-realizadas))

; Item 4 - Lista valor gasto por categoria
(pprint (compra/lista-valor-gasto-por-categoria (bd/lista-compras-realizadas)))

; Item 5 - Calcula valor da fatura em um determinado mês
(pprint (compra/valor-da-fatura-no-mes (bd/lista-compras-realizadas) "01"))
(pprint (compra/valor-da-fatura-no-mes (bd/lista-compras-realizadas) "02"))
(pprint (compra/valor-da-fatura-no-mes (bd/lista-compras-realizadas) "03"))

; Item 6 - Filtra compras
(pprint (compra/busca-compras-por-valor (bd/lista-compras-realizadas) 500M))
(pprint (compra/busca-compras-por-estabelecimento (bd/lista-compras-realizadas) "Centauro"))

