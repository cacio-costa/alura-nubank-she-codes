(ns semana-2.compra_test
  (:require [clojure.test :refer :all]
            [semana-2.compra :as compra]))

(deftest lista-valor-gasto-por-categoria-test
  (let [compras [{:data "01/01/2001", :valor 1500.0M, :estabelecimento "Outback", :categoria "Alimentação"}
                 {:data "02/01/2001", :valor 40.0M, :estabelecimento "Netflix", :categoria "Entretenimento"}
                 {:data "01/02/2001", :valor 400.0M, :estabelecimento "Cocobambu", :categoria "Alimentação"}

                 {:data "02/02/2001", :valor 260.0M, :estabelecimento "Posto de gasolina", :categoria "Combustível"}
                 {:data "03/02/2001", :valor 100.0M, :estabelecimento "Pizzaria", :categoria "Alimentação"}

                 {:data "01/03/2001", :valor 150.0M, :estabelecimento "Aramis", :categoria "Vestuário"}
                 {:data "02/03/2001", :valor 500.0M, :estabelecimento "Centauro", :categoria "Vestuário"}
                 {:data "03/03/2001", :valor 30.0M, :estabelecimento "Barbearia", :categoria "Beleza"}
                 {:data "04/03/2001", :valor 99.0M, :estabelecimento "Academia", :categoria "Saúde"}]

        compras-agrupadas (compra/lista-valor-gasto-por-categoria compras)]

    (testing "Deve agrupar gastos por categoria"
      (is (= (count compras-agrupadas) 6))

      (is (= 2000.0M (get compras-agrupadas "Alimentação")))
      (is (= 650.0M (get compras-agrupadas "Vestuário")))
      (is (= 260.0M (get compras-agrupadas "Combustível")))
      (is (= 40.0M (get compras-agrupadas "Entretenimento")))
      (is (= 30.0M (get compras-agrupadas "Beleza"))))))