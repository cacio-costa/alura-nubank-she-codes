(ns semana-2.banco-de-dados-test
  (:require [clojure.test :refer :all]
            [semana-2.banco-de-dados :as bd]
            [semana-2.compra :as compra]))

(deftest lista-compras-realizadas-test
  (bd/inicializa-banco)

  (testing "Deve listar as compras realizadas pelo cliente"
    (let [compras (bd/lista-compras-realizadas)]
      (is (= (count compras) 9))
      (is (= (last compras) {:data            "04/03/2001"
                             :valor           99.0M
                             :estabelecimento "Academia"
                             :categoria       "Saúde"})))))


(deftest testa-adiciona-compra
  (bd/inicializa-banco)

  (testing "Deve adicionar uma nova compra"
    (let [compra (compra/nova-compra "01/04/2021" 350.M "Contemporânea" "Música")
          lista-atualizada (bd/salva-compra compra)]

      (is (= (count lista-atualizada) 10))
      (is (= (last lista-atualizada) compra)))))
